import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ui-short-input',
  templateUrl: './short-input.component.html',
  styleUrls: ['./short-input.component.scss']
})
export class ShortInputComponent {
  /**
  * Output value of input field.
  */
   @Output() value = new EventEmitter<string>();

   /**
    * Label of input field.
    */
   @Input() label!: string;
 
   /**
    * Is input a password field?
    */
    @Input() password = false;
 
   /**
    * Has input value an error?
    */
    @Input() error = false;

  /**
    * Placeholder of input field.
    */
      @Input() placeholder = '';
 
   constructor() { }
 
   public valueChanged(event: Event):void{
       this.value.emit((event.target as HTMLInputElement).value);
   }

}
