import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ui-lighten-button',
  templateUrl: './lighten-button.component.html',
  styleUrls: ['./lighten-button.component.scss']
})
export class LightenButtonComponent {
  /**
   * Text of button.
   */
   @Input() text!: string;

   /**
    * Is button a warning button?
    */
    @Input() warning = false;

   /**
    * Icon displayed on the left side of the button text.
    */
    @Input() icon!:string;
 
   /**
    * Output of button is set to true if clicked.
    */
   @Output() isClicked = new EventEmitter<boolean>();
 
   constructor() { }
 
   public clicked():void{
     this.isClicked.emit(true);
   }
}
