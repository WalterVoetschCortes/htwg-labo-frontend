import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LightenButtonComponent } from './lighten-button.component';

describe('LightenButtonComponent', () => {
  let component: LightenButtonComponent;
  let fixture: ComponentFixture<LightenButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LightenButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LightenButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
