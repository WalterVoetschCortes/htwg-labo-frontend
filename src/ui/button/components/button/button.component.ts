import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  /**
   * Text of button.
   */
  @Input() text!: string;

  /**
   * Is button a warning button?
   */
   @Input() warning = false;

  /**
   * Output of button is set to true if clicked.
   */
  @Output() isClicked = new EventEmitter<boolean>();

  constructor() { }

  public clicked():void{
    this.isClicked.emit(true);
  }

}
