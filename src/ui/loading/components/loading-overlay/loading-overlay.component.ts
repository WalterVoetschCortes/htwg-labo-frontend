import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ui-loading-overlay',
  templateUrl: './loading-overlay.component.html',
  styleUrls: ['./loading-overlay.component.scss']
})
export class LoadingOverlayComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
