import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExerciseInfoComponent } from './components/exercise-info/exercise-info.component';
import { BackgroundModule, HeaderModule, LoadingModule } from '@ui';
import { TranslateModule } from '@ngx-translate/core';
import {MatTableModule} from '@angular/material/table'

@NgModule({
  declarations: [
    ExerciseInfoComponent
  ],
  imports: [
    CommonModule,
    BackgroundModule,
    LoadingModule,
    HeaderModule,
    TranslateModule,
    MatTableModule
  ]
})
export class ExerciseInfoModule { }
