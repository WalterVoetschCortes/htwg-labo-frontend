import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Exercise, ExerciseManagementService, ExerciseStepStatus, ExerciseStepStatusEnum, ExerciseWithSteps, LoadingService, User, UserManagementService } from '@shared';
import { BehaviorSubject, forkJoin, map, of, ReplaySubject, Subscription, switchMap } from 'rxjs';

interface TableEntry{
  participant: User,
  exerciseStepStatuses: ExerciseStepStatus[]
}

@Component({
  selector: 'app-exercise-info',
  templateUrl: './exercise-info.component.html',
  styleUrls: ['./exercise-info.component.scss']
})
export class ExerciseInfoComponent implements OnInit, OnDestroy {

  private _exerciseId!:number;
  private _subs = new Subscription();
  private _lecturerExercise$$ = new ReplaySubject<Exercise | null>(1);
  private _exerciseWithSteps$$ = new ReplaySubject<ExerciseWithSteps | null>(1);
  private _tableData$$ = new BehaviorSubject<TableEntry[]>([]);

  public lecturerExercise$ = this._lecturerExercise$$.asObservable();
  public exerciseWithSteps$ = this._exerciseWithSteps$$.asObservable();
  public tableData$ = this._tableData$$.asObservable();
  public loading$ = this._loadingService.loading$;
  public ExerciseStepStatus = ExerciseStepStatusEnum;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _exerciseManagementService: ExerciseManagementService,
    private _userManagementService: UserManagementService,
    private _loadingService: LoadingService
  ) { }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  ngOnInit(): void {
    const exerciseId = this._route.snapshot.params['exerciseId'];
    if(exerciseId){
      const id: number = +exerciseId;
      this._exerciseId = id; 
      this._subscribeToLecturerExercise(id);
      this._subscribeToExerciseWithSteps(id);
      this._subscribeToTableData(id); //this has to be called like every 3 minutes to be up to date
    }
  }

  public navigateToHome():void{
    this._router.navigate(['/home']);
  }

  public deleteExerciseWithSteps(){
    if(this._exerciseId){
      this._exerciseManagementService.deleteExerciseWithSteps(this._exerciseId);
    }
  }

  private _subscribeToLecturerExercise(exerciseId:number):void{
    this._subs.add(
      this._exerciseManagementService.getLectuterExercise(exerciseId).subscribe((lecturerExercise)=>{
        this._lecturerExercise$$.next(lecturerExercise);
      })
    );
  }

  private _subscribeToExerciseWithSteps(exerciseId:number):void{
    this._subs.add(
      this._exerciseManagementService.getExerciseWithSteps(exerciseId).subscribe((exerciseWithSteps)=>{
        this._exerciseWithSteps$$.next(exerciseWithSteps);
      })
    );
  }

  private _subscribeToTableData(exerciseId:number):void{
    this._subs.add(
      this._exerciseManagementService.getLectuterExercise(exerciseId).pipe(
        switchMap((exercise) => {
          if(exercise){
            return this._userManagementService.getCourseParticipants(exercise.courseid);
          }else{
            return [];
          }
        }),
        switchMap((participants) => {
          if(participants.length > 0){
            const tableData = participants.map((participant) => this._exerciseManagementService.getExerciseStepStatuses(exerciseId,participant.userid).pipe(
              map((exerciseStepStatuses) => {
                  const tableEntry: TableEntry = {
                    participant: participant,
                    exerciseStepStatuses: exerciseStepStatuses
                  }
                  return tableEntry;
              }),
            ));
            return forkJoin(tableData);                    
        }
        else return of([]);  
        })
      )
      .subscribe((tableData)=>{
        this._tableData$$.next(tableData);
      })
    );
  }
}
