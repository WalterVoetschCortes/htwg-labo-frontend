import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, ErrorService, LoadingService, LoginUser } from '@shared';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  private _loginUser: LoginUser = {
    username: '',
    password: ''
  };

  public error$ = this._errorService.error$;
  public loading$ = this._loadingService.loading$;

  constructor(
    private _authService: AuthService,
    private _errorService: ErrorService,
    private _loadingService: LoadingService,
    private _router: Router
  ) { }

  public usernameChanged(username: string): void{
    this._loginUser.username = username;
  }

  public passwordChanged(password: string): void{
    this._loginUser.password = password;
  }

  public login():void{
    this._authService.setTokenToLocalStorage(this._loginUser);
    this._loginUser = {
      username: '',
      password: ''
    };
  }

  public navigateToFaqs():void{
    this._router.navigate(['/login/faqs']);
  }
}
