import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private _router:Router,
    private _translateService: TranslateService
  ) { }

  ngOnInit(): void {
    // token
    const token = localStorage.getItem('token');
    if(!token){
      this._router.navigate(['/login']);
    }

    // translation
    const supportedLanguages = ['en', 'de']
    this._translateService.addLangs(supportedLanguages);
    const activeLang = localStorage.getItem('activeLang');
    if (activeLang && supportedLanguages.includes(activeLang)){
      this._translateService.use(activeLang);
    } else{
      this._translateService.use('en');
    }
  }
}
