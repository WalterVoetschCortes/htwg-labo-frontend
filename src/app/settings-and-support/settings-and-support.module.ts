import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsAndSupportComponent } from './components/settings-and-support/settings-and-support.component';
import { BackgroundModule, ButtonModule, HeaderModule, LoadingModule } from '@ui';
import { TranslateModule } from '@ngx-translate/core';
import { FaqsComponent } from './components/faqs/faqs.component';



@NgModule({
  declarations: [
    SettingsAndSupportComponent,
    FaqsComponent
  ],
  imports: [
    CommonModule,
    BackgroundModule,
    HeaderModule,
    ButtonModule,
    TranslateModule,
    LoadingModule
  ]
})
export class SettingsAndSupportModule { }
