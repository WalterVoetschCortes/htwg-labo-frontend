import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExerciseCreateStepsComponent } from './components/exercise-create-steps/exercise-create-steps.component';
import { BackgroundModule, ButtonModule, HeaderModule, InputModule, LoadingModule } from '@ui';
import { TranslateModule } from '@ngx-translate/core';
import { DragDropModule } from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [
    ExerciseCreateStepsComponent
  ],
  imports: [
    CommonModule,
    BackgroundModule,
    HeaderModule,
    TranslateModule,
    LoadingModule,
    DragDropModule,
    ButtonModule,
    InputModule
  ]
})
export class ExerciseCreateStepsModule { }
