import { Injectable, OnDestroy } from '@angular/core';
import { Exercise, ExerciseManagementService } from '@shared';
import { ReplaySubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExerciseCreateStepsService implements OnDestroy{

  private _subs = new Subscription();
  private _lecturerExercise$$ = new ReplaySubject<Exercise|null>();

  public lecturerExercise$ = this._lecturerExercise$$.asObservable();

  constructor(
    private _exerciseManagementService: ExerciseManagementService
  ) { }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  public subscribeToLecturerExercise(exerciseId:number):void{
    this._subs.add(
      this._exerciseManagementService.getLectuterExercise(exerciseId).subscribe((lecturerExercise)=>{
        this._lecturerExercise$$.next(lecturerExercise);
      })
    );
  }
}
