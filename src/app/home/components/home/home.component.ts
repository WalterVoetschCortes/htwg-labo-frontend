import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingService, User, UserManagementService } from '@shared';
import { ReplaySubject, Subscription } from 'rxjs';
import { HomeService } from '../../services/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  private _subs = new Subscription();
  private _user$$ = new ReplaySubject<User | null>(1);

  public lecturerExercises$ = this._homeService.lecturerExercises$;
  public studentExercises$ = this._homeService.studentExercises$;
  public user$ = this._user$$.asObservable();
  public loading$ = this._loadingService.loading$;

  constructor(
    private _router: Router,
    private _loadingService: LoadingService,
    private _userManagementService: UserManagementService,
    private _homeService: HomeService
  ) { }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  ngOnInit(): void {
    this._homeService.subscribeToLecturerExercises();
    this._homeService.subscribeToStudentExercises();
    this._subscribeToUser();
  }

  public navigateToSettingsAndSupport():void {
    this._router.navigate(['/settings-and-support']);
  }

  public navigateToLecturerExercise(exerciseId:number, hasDefinedSteps:boolean){
    if(hasDefinedSteps){
      this._router.navigate(['/exercise/info/' + exerciseId]);
    } else {
      this._router.navigate(['/exercise/create-steps/' + exerciseId]);
    }
  }

  public navigateToStudentExercise(exerciseId:number){
    this._router.navigate(['/exercise/set-state/' + exerciseId]);
  }

  private _subscribeToUser():void{
    this._subs.add(
      this._userManagementService.getUser().subscribe((user)=>{
        this._user$$.next(user);
      })
    );
  }
}
