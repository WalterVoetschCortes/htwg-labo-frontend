import { Injectable, OnDestroy } from '@angular/core';
import { Exercise, ExerciseManagementService } from '@shared';
import { ReplaySubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService implements OnDestroy{
  private _subs = new Subscription();
  private _lecturerExercises$$ = new ReplaySubject<Exercise[]>(1);
  private _studentExercises$$ = new ReplaySubject<Exercise[]>(1);

  public lecturerExercises$ = this._lecturerExercises$$.asObservable();
  public studentExercises$ = this._studentExercises$$.asObservable();

  constructor(
    private _exerciseManagementService: ExerciseManagementService
  ) {

  }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  public subscribeToLecturerExercises():void{
    this._subs.add(
      this._exerciseManagementService.getLecturerExercises().subscribe((lecturerExercises)=>{
        this._lecturerExercises$$.next(lecturerExercises);
      })
    );
  }

  public subscribeToStudentExercises():void{
    this._subs.add(
      this._exerciseManagementService.getStudentExercises().subscribe((studentExercises)=>{
        this._studentExercises$$.next(studentExercises);
      })
    );
  }
}
