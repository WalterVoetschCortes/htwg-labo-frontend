import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { environment } from '@env';
import { AuthService, Exercise, ExerciseStepStatus, ExerciseStepStatusEnum, ExerciseWithSteps } from '@shared';
import { UserManagementService } from '@shared';
import { ErrorService } from '@shared';
import { LoadingService } from '@shared';
import { catchError, combineLatestWith, forkJoin, map, Observable, of, ReplaySubject, Subscription, switchMap, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExerciseManagementService implements OnDestroy{

  private _subs = new Subscription();
  private _errorText = '';

  constructor(
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _authService: AuthService,
    private _httpClient: HttpClient,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _userManagementService: UserManagementService
  ) { }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  // Gets exercises of the lecturer from backend
  public getLecturerExercises():Observable<Exercise[]>{
    this._loadingService.loading$$.next(true);
    this._errorService.error$$.next(false);
    const loginToken = this._authService.getToken();
    return this._httpClient.post<Exercise[]>(environment.backendBaseUrl + '/exercise-management/lecturer-exercises', loginToken).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Du hast keine Berechtigungen...'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            } 
            this._snackBar.open(this._errorText, undefined, {
              duration: 5000,
              panelClass: ['red-snackbar'],
            });
            this._errorText='';
            return of([]);
          }),
          tap(() => {
            this._loadingService.loading$$.next(false);
          })
      )
  }

  // Gets exercises of the student from backend
  public getStudentExercises():Observable<Exercise[]>{
    this._loadingService.loading$$.next(true);
    this._errorService.error$$.next(false);
    const loginToken = this._authService.getToken();
    return  this._httpClient.post<Exercise[]>(environment.backendBaseUrl + '/exercise-management/student-exercises', loginToken).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Du hast keine Berechtigungen...'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              }
            } 
            this._snackBar.open(this._errorText, undefined, {
              duration: 5000,
              panelClass: ['red-snackbar'],
            });
            this._errorText='';
            return of([]);
          }),
          tap(() => {
            this._loadingService.loading$$.next(false);
          })
      )
  }

  // Get one lecturer exercise from backend
  public getLectuterExercise(exerciseId:number):Observable<Exercise|null>{
    const loginToken = this._authService.getToken();
    const body = {
      token: loginToken.token,
      exerciseId: exerciseId
    }
    this._loadingService.loading$$.next(true);
    this._errorService.error$$.next(false);
    return this._httpClient.post<Exercise>(environment.backendBaseUrl + '/exercise-management/lecturer-exercise', body).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Du hast keine Berechtigungen...'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            } 
            this._snackBar.open(this._errorText, undefined, {
              duration: 5000,
              panelClass: ['red-snackbar'],
            });
            this._errorText='';
            return of(null);
          }),
          tap(() => {
            this._loadingService.loading$$.next(false);
          })
      )
  }

  // Delete an exercise with steps
  public deleteExerciseWithSteps(exerciseId: number):void{
    this._subs.add(
      this._httpClient.post<Exercise>(environment.backendBaseUrl + '/exercise-management/deleteExerciseWithSteps', {exerciseId: exerciseId}).pipe(
        catchError(error => {
          switch(error.status) { 
            case 401: { 
              this._errorText = 'Du hast keine Berechtigungen...'
              this._errorService.error$$.next(true);
              break; 
            } 
            case 0: { 
              this._errorText = 'Server Error: ' + error.message
              break; 
            } 
            default: { 
              this._errorText = 'Unknown error'
                break; 
            } 
          } 
          return of(null);
        }),
    ).subscribe((exerciseWithSteps)=> {
      this._loadingService.loading$$.next(false);
      if(exerciseWithSteps){
        this._router.navigate(['/home']);
        this._snackBar.open('Übungsschritte und Übungsstand wurden erfolgreich gelöscht.', undefined, {
          duration: 5000,
          panelClass: ['green-snackbar'],
        });    
      } else {
        this._snackBar.open(this._errorText, undefined, {
          duration: 5000,
          panelClass: ['red-snackbar'],
        });
        this._errorText='';
      }
    })
    );
  }

  // Insert a new exercise with steps to backend
  public addNewExerciseWithSteps(exerciseWithSteps: ExerciseWithSteps):void{
    if(exerciseWithSteps.steps.length <= 0){
      this._snackBar.open('Definiere mindestens ein Übungsschritt!', undefined, {
        duration: 5000,
        panelClass: ['red-snackbar'],
      });
      return;
    }
    let error = false;
    exerciseWithSteps.steps.forEach((step)=> {
      if(step.name.trim() === ''){
        this._snackBar.open('Der Name muss in jedem Übungsschritt definiert sein!', undefined, {
          duration: 5000,
          panelClass: ['red-snackbar'],
        });
        error = true;
        return;
      }
    });
    if(error){
      return;
    }
    this._loadingService.loading$$.next(true);
    this._errorService.error$$.next(false);
    this._subs.add(
      this._httpClient.post<Exercise>(environment.backendBaseUrl + '/exercise-management/insertExerciseWithSteps', exerciseWithSteps).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Du hast keine Berechtigungen...'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            } 
            return of(null);
          }),
      ).subscribe((exerciseWithSteps)=> {
        this._loadingService.loading$$.next(false);
        if(exerciseWithSteps){
          this._router.navigate(['/home']);
          this._snackBar.open('Übungsschritte wurden erfolgreich definiert', undefined, {
            duration: 5000,
            panelClass: ['green-snackbar'],
          });    
        } else {
          this._snackBar.open(this._errorText, undefined, {
            duration: 5000,
            panelClass: ['red-snackbar'],
          });
          this._errorText='';
        }
      })
    );
  }

  // Get exercise with steps
  public getExerciseWithSteps(exerciseId:number):Observable<ExerciseWithSteps | null>{
    this._loadingService.loading$$.next(true);
    this._errorService.error$$.next(false);
    return this._httpClient.post<ExerciseWithSteps>(environment.backendBaseUrl + '/exercise-management/getExerciseWithSteps', {exerciseId: exerciseId}).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Du hast keine Berechtigungen...'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            }
            this._snackBar.open(this._errorText, undefined, {
              duration: 5000,
              panelClass: ['red-snackbar'],
            });
            this._errorText='';
            return of(null);
          }),
          tap(() => {
            this._loadingService.loading$$.next(false);
          })
      )
  }

  // Gets statuses of all steps of an exercise with id
  public getExerciseStepStatuses(exerciseId:number, userId?: number):Observable<ExerciseStepStatus[]>{
    const body = {
      exerciseId: exerciseId
    }
    this._loadingService.loading$$.next(true);
    this._errorService.error$$.next(false);
    return this._httpClient.post<ExerciseWithSteps>(environment.backendBaseUrl + '/exercise-management/getExerciseWithSteps', body).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Du hast keine Berechtigungen...'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            } 
            this._snackBar.open(this._errorText, undefined, {
              duration: 5000,
              panelClass: ['red-snackbar'],
            });
            this._errorText='';
            return of(null);
          }),
          combineLatestWith(this._userManagementService.getUser()),
          switchMap(([exercise, user]) => {
            let userid = userId;
            if(userid == null ){
              userid = user?.userid;
            }
            if(exercise){
              if(exercise.steps.length > 0 && user){
                  const newExercise = exercise.steps.map((exerciseStep) => this._httpClient.post<ExerciseStepStatus>(environment.backendBaseUrl + '/exercise-management/getExerciseStepStatus', {exerciseId: exercise.exerciseId, exerciseStepId: exerciseStep._id, userId: userid}).pipe(
                    catchError(error => {
                      switch(error.status) { 
                        case 401: { 
                          this._errorText = 'Du hast keine Berechtigungen...'
                          this._errorService.error$$.next(true);
                          break; 
                        } 
                        case 0: { 
                          this._errorText = 'Server Error: ' + error.message
                          break; 
                        } 
                        default: { 
                          this._errorText = 'Unknown error'
                            break; 
                        } 
                      } 
                      return of(null);
                    }),
                    map((res) => {
                      let exerciseStepStatus: ExerciseStepStatus = {
                        name: exerciseStep.name,
                        description: exerciseStep.description,
                        order: exerciseStep.order,
                        userId: user.userid,
                        exerciseId: exercise.exerciseId,
                        status: res === null ? ExerciseStepStatusEnum.TODO : res.status,
                      };
                      if(exerciseStep._id){
                        exerciseStepStatus = {
                          ...exerciseStepStatus,
                          exerciseStepId: exerciseStep._id,
                        }
                      }
                        return exerciseStepStatus;
                    }),
                  ));
                  return forkJoin(newExercise);                    
              }
              else return of([]);              
            }
            else return of([]);              
        }),
        tap(() => {
          this._loadingService.loading$$.next(false);
        })
      )
  }

  // Gets status of an exercise step with id
  public getExerciseStepStatus(exerciseId:number, stepId:string):Observable<ExerciseStepStatus|undefined|null>{
    this._loadingService.loading$$.next(true);
    this._errorService.error$$.next(false);
    return this.getExerciseStepStatuses(exerciseId).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Du hast keine Berechtigungen...'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            }
            this._snackBar.open(this._errorText, undefined, {
              duration: 5000,
              panelClass: ['red-snackbar'],
            });
            this._errorText='';
            return of(null);
          }),
          map(exerciseWithSteps => {
            if(exerciseWithSteps){
              return exerciseWithSteps.find((step) => step.exerciseStepId == stepId)
            }
            else return null;
          }),
          tap(() => {
            this._loadingService.loading$$.next(false);
          })
      )
  }

  // Updates an exercise step status
  public updateExerciseStepStatus(exerciseStepStatus: ExerciseStepStatus, name: string, description:string, order: number):Observable<ExerciseStepStatus | null>{
    this._loadingService.loading$$.next(true);
    this._errorService.error$$.next(false);
    return  this._httpClient.post<ExerciseStepStatus>(environment.backendBaseUrl + '/exercise-management/upsertExerciseStepStatus', exerciseStepStatus).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Du hast keine Berechtigungen...'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            } 
            this._snackBar.open(this._errorText, undefined, {
              duration: 5000,
              panelClass: ['red-snackbar'],
            });
            this._errorText='';
            return of(null);
          }),
          map((exerciseStepStatus) => {
            if(exerciseStepStatus){
              const newExerciseStepStatus:ExerciseStepStatus = {
                ...exerciseStepStatus,
                name: name,
                description: description,
                order: order
              };
              return newExerciseStepStatus;
            }
            return null;
          }),
          tap(() => {
            this._loadingService.loading$$.next(false);
          })
      )
  }

  // Get one student exercise
  public getStudentExercise(exerciseId:number):Observable<Exercise | null>{
    const loginToken = this._authService.getToken();
    const body = {
      token: loginToken.token,
      exerciseId: exerciseId
    }
    this._loadingService.loading$$.next(true);
    this._errorService.error$$.next(false);
    return  this._httpClient.post<Exercise>(environment.backendBaseUrl + '/exercise-management/student-exercise', body).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Du hast keine Berechtigungen...'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            } 
            this._snackBar.open(this._errorText, undefined, {
              duration: 5000,
              panelClass: ['red-snackbar'],
            });
            this._errorText='';
            return of(null);
          }),
          tap(() => {
            this._loadingService.loading$$.next(false);
          })
      )
  }
}
