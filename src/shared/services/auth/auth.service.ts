import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { environment } from '@env';
import { ErrorService, LoginToken, User } from '@shared';
import { LoadingService } from '@shared';
import { LoginUser } from '@shared';
import { catchError, of, ReplaySubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy{

  private _subs = new Subscription();
  private _errorText = '';
  private _user$$ = new ReplaySubject<User>();
  
  public user$ = this._user$$.asObservable();

  constructor(
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _httpClient: HttpClient, 
    private _router: Router,
    private _snackBar: MatSnackBar
  ) {
  }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  // Get token from backend and store it localStorage:
  public setTokenToLocalStorage(loginUser: LoginUser):void{
    this._loadingService.loading$$.next(true);
    this._errorService.error$$.next(false);
    this._subs.add(
      this._httpClient.post<LoginToken>(environment.backendBaseUrl + '/auth/token', loginUser).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Login ist fehlgeschlagen. Versuche es nochmal!'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            } 
            return of(null);
          }),
      ).subscribe((token)=> {
        this._loadingService.loading$$.next(false);
        if(token && token?.token){
          localStorage.setItem ('token', token.token);
          this._router.navigate(['/home']);
        } else {
          this._snackBar.open(this._errorText, undefined, {
            duration: 5000,
            panelClass: ['red-snackbar'],
          });
          this._errorText='';
        }
      })
    );
  }

  //Get token from localStorage
  public getToken(): LoginToken{
    const tokenString = localStorage.getItem('token');
    let token: LoginToken = {
      token: ''
    }
    if(tokenString){
      token.token = tokenString;
    }else {
      this._router.navigate(['/login']);
    }
    return token;
  }
}
