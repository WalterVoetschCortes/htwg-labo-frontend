import { ExerciseStepStatusEnum } from "@shared";

export interface ExerciseStepStatus {
    _id?: string,
    name?: string,
    description?:string,
    order?:number,
    exerciseStepId?: string,
    userId: number,
    status: ExerciseStepStatusEnum,
    exerciseId: number
}