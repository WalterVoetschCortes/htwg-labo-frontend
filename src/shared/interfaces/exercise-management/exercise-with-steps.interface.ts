export interface ExerciseWithSteps{   
    _id?: string;
    exerciseId: number;
    steps: ExerciseStep[];
}

export interface ExerciseStep{  
    _id?: string;
    name: string;
    description: string;
    order: number;
}