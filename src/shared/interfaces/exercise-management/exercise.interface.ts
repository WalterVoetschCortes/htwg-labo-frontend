export interface Exercise {
    courseid: number;
    coursefullname: string;
    courseshortname: string;
    id: number;
    name: string;
    duedate: number;
    allowsubmissionsfromdate: number;
    intro: string;
    hasDefinedSteps: boolean;
}